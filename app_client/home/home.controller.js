(function(){
    angular.module('loc8rApp')
            .controller('homeCtrl',homeCtrl);
    homeCtrl.$inject=['$scope','loc8rData','geolocation'];
    function homeCtrl(a,b,c){
        var vm = this;
        vm.pageHeader = {
            title: 'Loc8r',
            strapline: 'Find places to work with wifi near you!'
        };
        vm.sidebar = {
            content: "Looking for wifi and a seat? Etc etc..."
        };
        vm.message = "Checking your location";
        vm.getData = function (position) {
            vm.message = "Searching locations near you";
            var lat = position.coords.latitude, lng = position.coords.longitude;
            console.log(position);
            b.locationByCoords(lat, lng).success(function (data) {
                vm.message = data.length > 0 ? "The following locations are near you!" : "No locations found";
                vm.data = {locations: data};
            }).error(function (e) {
                console.log(e);
                vm.message = "Sorry, something went wrong. Try again later";
            });
        };

        vm.showError = function (error) {
            a.$apply(function () {
                vm.message = error.message;
            });
        };

        vm.noGeo = function () {
            a.$apply(function () {
                vm.message = "Geolocation not supported";
            });
        };
        c.getPosition(vm.getData, vm.showError, vm.noGeo);
    }
})();