$('#addReview').submit(function (e) {
  $('.card.red').hide();
  if (!$('input#name').val() || !$('select#rating').val() || !$('textarea#review').val()) {
    if ($('.card.red').length) {
      $('.card.red').show();
    } else {
      $(this).prepend('<div class="row"><div class="col m6 s12"><div class="card red"><div class="card-content white-text"><span class="card-title">Attention</span><p>All fields required, please try again</p></div></div></div></div>');
    }
    return false;
  }
});