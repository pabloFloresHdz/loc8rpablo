var rH=require('./helpers/responseHelpers');
var mongoose=require('mongoose');
var Loc=mongoose.model('Location'); 
var theEarth=(function(){
  var earthRadius=6371;
  var getDistanceFromRads=function(rads){
    return parseFloat(earthRadius*rads);
  };
  var getRadsFromDistance=function(distance){
    return parseFloat(distance/earthRadius);
  };
  return {
    getDistanceFromRads:getDistanceFromRads,
    getRadsFromDistance:getRadsFromDistance
  };
})();

var getLocationsList=function(data){
  var locs=[];
  data.forEach(function(doc){
    locs.push({
      distance:theEarth.getDistanceFromRads(doc.dis),
      name:doc.obj.name,
      address:doc.obj.address,
      rating:doc.obj.rating,
      facilities:doc.obj.facilities,
      _id:doc.obj._id
    });
  });
  return locs;
};

module.exports.locationsCreate=function(req,res){
  Loc.create({
    name:req.body.name,
    address:req.body.address,
    facilities:req.body.facilities.split(","),
    coords:[parseFloat(req.body.lng),parseFloat(req.body.lat)],
    openingTimes:[{
        days:req.body.days1,
        opening:req.body.opening1,
        closing:req.body.closing1,
        closed:req.body.closed1
        },{
        days:req.body.days2,
        opening:req.body.opening2,
        closing:req.body.closing2,
        closed:req.body.closed2
        }
    ]
  },function(err,location){
    if(err){
      rH.sendJsonResponse(res,400,err);
      return;
    }
    rH.sendJsonResponse(res,201,location);
  });
};

module.exports.locationsListByDistance=function(req,res){
  var lng=parseFloat(req.query.lng);
  var lat=parseFloat(req.query.lat);
  if(!lng && lng!== 0||!lat && lng!== 0){
    rH.sendJsonResponse(res,404,{"message":"lng and lat params are required"});
    return;
  }
  var point={
    type:'Point',
    coordinates:[lng,lat]
  };
  var geoOptions={
    spherical:true, 
    maxDistance:theEarth.getRadsFromDistance(parseFloat(req.query.maxDistance||20)),//max distance 20km
    num:10 //like sql limit
  };
  Loc.geoNear(point,geoOptions,function(err,results,stats){
    if(err){
      rH.sendJsonResponse(res,404,err);
      return;
    }
    console.log('Loc Query');
    console.log(results);
    var locations=getLocationsList(results);
    rH.sendJsonResponse(res,200,locations);
  });
};

module.exports.locationsReadOne=function(req,res){
  if(req.params && req.params.locationId){
    Loc.findById(req.params.locationId)
            .exec(function(err,location){
              if(err){
                rH.sendJsonResponse(res,404,err);
                return;
              }else if(!location){
                rH.sendJsonResponse(res,404,{"message":"No location found"});
                return;
              }
              rH.sendJsonResponse(res,200,location);
            });
  }else{
    rH.sendJsonResponse(res,404,{"message":"No location id in request"});
  }
};

module.exports.locationsUpdateOne=function(req,res){
  if(!req.params.locationId){
    rH.sendJsonResponse(res,404,{"message":"Not found, location id required"});
    return;
  }
  Loc.findById(req.params.locationId)
      .select('-reviews -rating')
      .exec(function(err,location){
        if(err){
          rH.sendJsonResponse(res,400,err);
          return;
        }
        if(!location){
          rH.sendJsonResponse(res,404,{"message":'Location not found'});
          return;
        }
        location.name=req.body.name;
        location.address=req.body.address;
        location.facilities=req.body.facilities.split(',');
        location.coords=[parseFloat(req.body.lng),parseFloat(req.body.lat)];
        location.openingTimes=[{
                days:req.body.days1,
                opening:req.body.opening1,
                closing:req.body.closing1,
                closed:req.body.closed1
                },{
                days:req.body.days2,
                opening:req.body.opening2,
                closing:req.body.closing2,
                closed:req.body.closed2
                }
              ];
        location.save(function(err,location){
          if(err){
            rH.sendJsonResponse(res,404,err);
          }else{
            rH.sendJsonResponse(res,200,location);
          }
        });
      });
};

module.exports.locationsDeleteOne=function(req,res){
  if(req.params && req.params.locationId){
    Loc.findByIdAndRemove(req.params.locationId)
            .exec(function(err,location){
              if(err){
                rH.sendJsonResponse(res,404,err);
                return;
              }
              rH.sendJsonResponse(res,204,null);
            });
  }else{
    rH.sendJsonResponse(res,404,{"message":"No location id in request"});
  }
};
