/*Requires*/
var rH=require('./helpers/responseHelpers');
var mongoose=require('mongoose');
var Loc=mongoose.model('Location'); 

var updateAverageRating=function(id){
  Loc
      .findById(id)
      .select('rating reviews')
      .exec(
        function(err,location){
          if(!err){
            doSetAverageRating(location)
          }
        }
      );
};

var doSetAverageRating=function(location){
  var i,reviewCount,ratingAverage,ratingTotal;
  if(location.reviews && location.reviews.length>0){
    reviewCount=location.reviews.length;
    ratingTotal=0;
    for(i=0;i<reviewCount;i++){
      ratingTotal=ratingTotal+location.reviews[i].rating;
    }
    ratingAverage=parseInt(ratingTotal/reviewCount);
    location.rating=ratingAverage;
    location.save(function(err){
      if(err){
        console.log(err);
      }else{
        console.log("Average updated");
      }
    });
  }
};

var doAddReview=function(req,res,location){
  if(!location){
    rH.sendJsonResponse(res,400,{message:"Location not found"});
  }else{
    location.reviews.push({
      author:req.body.author,
      rating:req.body.rating,
      reviewText:req.body.reviewText
    });
    location.save(function(err,location){
      var thisReview;
      if(err){
        console.log(err);
        rH.sendJsonResponse(res,400,err);
      }
      else{
        updateAverageRating(location._id);
        thisReview=location.reviews[location.reviews.length-1];
        rH.sendJsonResponse(res,201,thisReview);
      }
    });
  }
};

/*Specific controller code*/
module.exports.reviewsCreate=function(req,res){
  var locId=req.params.locationId;
  if(locId){
    Loc.findById(locId)
            .select('reviews')
            .exec(function(err,location){
              if(err){
                rH.sendJsonResponse(res,400,err);
              }else{
                doAddReview(req,res,location);
              }
            });
  }else{
    rH.sendJsonResponse(res,404,{message:"Not found, locationId required"});
  }
};

module.exports.reviewsReadOne=function(req,res){
  if(req.params && req.params.locationId && req.params.reviewId){
    Loc.findById(req.params.locationId)
            .select('name reviews')//projection
            .exec(function(err,location){
              if(err){
                rH.sendJsonResponse(res,404,err);
                return;
              }else if(!location){
                rH.sendJsonResponse(res,404,{"message":"No location found"});
                return;
              }
              if(location.reviews && location.reviews.length>0){
                review=location.reviews.id(req.params.reviewId);
                if(!review){
                  rH.sendJsonResponse(res,404,{"message":"No review found with the id provided"});
                }else{
                  rH.sendJsonResponse(res,200,{
                    location:{
                      name:location.name,
                      id:req.params.locationId
                    },
                    review:review
                  });
                }
              }else{
                rH.sendJsonResponse(res,404,{"message":"No reviews found"});
              }
            });
  }else{
    rH.sendJsonResponse(res,404,{"messae":"No location id in request"});
  }
};

module.exports.reviewsUpdateOne=function(req,res){
  if(req.params.locationId && req.params.reviewId){
    Loc
      .findById(req.params.locationId)
      .select('reviews')
      .exec(function(err,location){
        if(err){
          rH.sendJsonResponse(res,400,err);
          return;
        }
        if(!location){
          rH.sendJsonResponse(res,404,{message:"Location not found"});
          return;
        }
        var thisReview;
        if(location.reviews && location.reviews.length>0){
          thisReview=location.reviews.id(req.params.reviewId);
          if(!thisReview){
            rH.sendJsonResponse(res,404,{"message":"Review not found"});
          }else{
            thisReview.author=req.body.author;
            thisReview.rating=req.body.rating;
            thisReview.reviewText=req.body.reviewText;
            location.save(function(err,location){
              if(err){
                rH.sendJsonResponse(res,400,err);
              }else{
                updateAverageRating(location._id);
                rH.sendJsonResponse(res,200,thisReview);
              }
            });
          }
        }
      });
      
  }else{
    rH.sendJsonResponse(res,404,{"message":"Can't find review, location id and review id required"});
    return;
  }
};

module.exports.reviewsDeleteOne=function(req,res){
  if(req.params.locationId && req.params.reviewId){
    Loc
      .findById(req.params.locationId)
      .select('reviews')
      .exec(function(err,location){
        if(err){
          rH.sendJsonResponse(res,400,err);
          return;
        }
        if(!location){
          rH.sendJsonResponse(res,404,{message:"Location not found"});
          return;
        }
        var thisReview;
        if(location.reviews && location.reviews.length>0){
          thisReview=location.reviews.id(req.params.reviewId);
          if(!thisReview){
            rH.sendJsonResponse(res,404,{"message":"Review not found"});
          }else{
            location.reviews.id(req.params.reviewId).remove();
            location.save(function(err){
              if(err){
                rH.sendJsonResponse(res,404,err);
              }else{
                updateAverageRating(location._id);
                rH.sendJsonResponse(res,204,null);
              }
            });
          }
        }
      });
      
  }else{
    rH.sendJsonResponse(res,404,{"message":"Can't find review, location id and review id required"});
    return;
  }
};
