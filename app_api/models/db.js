var mongoose=require('mongoose');
var dbURI='mongodb://localhost/Loc8r';
if(process.env.NODE_ENV==='production'){
  dbURI=process.env.MONGOLAB_URI;
}
//var dbConn1=mongoose.createConnection(dbUri);
//dbConn1.on...
mongoose.connect(dbURI);
mongoose.connection.on('connected',function(){
  console.log('Connected to:'+dbURI);
});
mongoose.connection.on('error',function(err){
  console.log('Mongoose connection error:'+err);
});
mongoose.connection.on('disconnected',function(){
  console.log('Disconnected from DB');
});

var gracefulShutdown=function(msg,callback){
  mongoose.connection.close(function(){
    console.log('Mongoose disconnected throught '+msg);
    callback();
  });
};

//Event,callback
process.once('SIGUSR2',function(){//nodemon
  gracefulShutdown('nodemon restart',function(){
    process.kill(process.pid,'SIGUSR2');
  });
});

process.on('SIGINT',function(){//nodejs app termination
  gracefulShutdown('app termination',function(){
    process.exit(0);
  });
});

process.on('SIGTERM',function(){//heroku shutdown
  gracefulShutdown('Heroku app shutdown',function(){
    process.exit(0);
  });
});
require('./locations');