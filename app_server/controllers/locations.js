var request=require('request');
var apiOptions={
  server:"http://localhost:3000/"
};
if(process.env.NODE_ENV==='production'){
  apiOptions.server='http://pfh-loc8r.herokuapp.com/';
}

var renderHomepage=function(req,res){//,body){
  var message="";
//  if(!(body instanceof Array)){
//    message="API lookup error";
//    body=[];
//  }else{
//    if(!body.length){
//      message="No places found nearby";
//    }
//  }
  res.render('locations-list',{
    title:'Find a place to work with wifi',
    pageHeader:{
      title:'Loc8r',
      strapline:'Find places to work with wifi near you'
    },
    sidebar:"Looking for wifi and a seat? Loc8r helps you find places to work when out and about. Perhaps with coffee, cake or a pint? Let Loc8r help you find the place you\'re looking for.",
    //locations:body,
    message:message
  });
};

var _formatDistance=function(distance){
  var numDistance,unit;
  if(distance>1){
    numDistance=parseFloat(distance).toFixed(1);
    unit='km';
  }else{
    numDistance=parseInt(distance*1000,10);
    unit='m';
  }
  return numDistance+unit;
};

var renderDetailPage=function(req,res,data){
  res.render('location-info',{
    title:data.name,
    pageHeader:{title:data.name},
    sidebar:{
      context:'is on Loc8r because it has accessible wifi\n\
and space to sitdown with your laptop and get some work done.',
      callToAction:'If you\'ve been and you like \n\
it - or if you don\'t -please leave a review to help other people just like you.'
    },
    location:data
  });
};

var _showError=function(req,res,statusCode){
  var title,content;
  if(statusCode===404){
    title="404, page not found";
    content="Holly cow!, it looks like the page you were looking for doesn't exist";
  }else{
    title=statusCode + ", something's gone wrong";
    content="Something, somewhere, has gone just a little bit wrong ";
  }
  res.status(statusCode);
  res.render('generic-text',{
    title:title,
    content:content
  });
};

var renderReviewForm=function(req,res,locDetail){
    console.log(req.originalUrl);
  res.render('add-review',{
    title:'Review '+locDetail.name+' on Loc8r',
    pageHeader: {title:'Review '+locDetail.name},
    error:req.query.err,
    url:req.originalUrl
  });
};

var getLocationInfo=function(req,res,callback){
  var requestOptions,path;
  path='api/locations/'+req.params.locationId;
  requestOptions={
    url:apiOptions.server+path,
    method:"GET",
    json:{}
  };
  request(requestOptions,function(err,response,body){
    var data=body;
    if(response.statusCode===200){
      data.coords={
        lng:body.coords[0],
        lat:body.coords[1]
      };
      callback(req,res,data);
    }else{
      _showError(req,res,response.statusCode);
    }
  });
};

/* GET homepage */
module.exports.homeList=function(req,res){
//  var requestOptions,path;
//  path='api/locations';
//  requestOptions={
//    url:apiOptions.server+path,
//    method:"GET",
//    json:{},
//    qs:{
//      lng : -0.9690884,
//      lat : 51.455041,
//      maxDistance : 20
//    }
//  };
//  request(requestOptions,function(err,response,body){
//    var i,data;
//    data=body;
//    if(response.statusCode===200 && data.length){
//      for(i=0;i<data.length;i++){
//        data[i].distance=_formatDistance(data[i].distance);
//      }
//    }
//    if(err){
//      console.log(err);
//    }
    renderHomepage(req,res);//,data);
//  });
};

/* GET homepage */
module.exports.locationInfo=function(req,res){
  getLocationInfo(req,res,function(req,res,responseData){
    renderDetailPage(req,res,responseData);
  });    
};

/* GET Add review page */
module.exports.addReview=function(req,res){
  getLocationInfo(req,res,function(req,res,responseData){
    renderReviewForm(req,res,responseData);
  });
};

module.exports.doAddReview=function(req,res){
  var requestOptions,path,locationId,postdata;
  locationId=req.params.locationId;
  path="api/locations/"+locationId+'/reviews';
  postdata={
    author:req.body.name,
    rating:parseInt(req.body.rating,5),
    reviewText:req.body.review
  };
  requestOptions={
    url:apiOptions.server+path,
    method:"POST",
    json:postdata
  };
  if(!postdata.author||!postdata.rating||!postdata.reviewText)
    res.redirect('/location/'+locationId+'/review/new?err=val')
  else
  request(
    requestOptions,
    function(err,response,body){
      if(response.statusCode===201){
        res.redirect('/location/'+locationId);
      }else if(response.statusCode===400 && body.name && body.name==='ValidationError'){
        res.redirect('/location/'+locationId+'/review/new?err=val')
      }
      else{
        _showError(req,res,response.statusCode);
      }
    }
  );
};
